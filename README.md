Requirements
------------

  * PHP 8.0 or higher;
  * PDO-SQLite PHP extension enabled;
  * Composer;

```bash
$ composer install
```

Create .env.local file with credentials provideds to you

```bash
$ php -S localhost:8000 -t public/
```
