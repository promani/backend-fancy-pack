<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait CreatedBy
{
	#[ORM\Column(type: 'string', nullable: true)]
    protected ?string $createdBy;

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy)
    {
        $this->createdBy = $createdBy;
    }
}
