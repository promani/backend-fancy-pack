<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
	collectionOperations: ['post'],
	itemOperations: ['get','put','patch','delete'],
	denormalizationContext: ['groups' => ['write']],
	normalizationContext: ['groups' => ['read']],
)]
#[ORM\HasLifecycleCallbacks]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
	use Timestamp;

	#[ORM\Column(type: 'integer')]
	#[ORM\Id]
	#[ORM\GeneratedValue(strategy: 'AUTO')]
	#[Groups('read')]
	private ?int $id = null;

	#[ORM\Column(type: 'string', unique: true)]
	#[Groups(['read','write'])]
	private ?string $username = '';

	#[ORM\Column(type: 'string')]
	#[Groups('write')]
	private ?string $password = '';

	#[ORM\OneToMany(mappedBy: 'user', targetEntity: Todo::class)]
	#[ApiSubresource]
	private Collection $todos;

	public function getRoles(): array
	{
		return ['ROLE_USER'];
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getPassword(): ?string
	{
		return $this->password;
	}

	public function setPassword(?string $password): void
	{
		$this->password = $password;
	}

	public function getUsername(): ?string
	{
		return $this->username;
	}

	public function setUsername(?string $username): void
	{
		$this->username = $username;
	}

	public function getUserIdentifier(): string
	{
		return $this->username;
	}

	public function getTodos(): ArrayCollection|Collection
	{
		return $this->todos;
	}

	public function setTodos(ArrayCollection|Collection $todos): void
	{
		$this->todos = $todos;
	}


	public function getSalt()
	{}

	public function eraseCredentials()
	{}
}
