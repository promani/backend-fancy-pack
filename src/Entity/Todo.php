<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
	collectionOperations: ['get','post'],
	itemOperations: ['get','put','patch','delete'],
	denormalizationContext: ['groups' => ['write']],
	normalizationContext: ['groups' => ['read']],
)]
#[ORM\HasLifecycleCallbacks]
class Todo
{
	use Timestamp;

	#[ORM\Column(type: 'integer')]
	#[ORM\Id]
	#[ORM\GeneratedValue(strategy: 'AUTO')]
	#[Groups('read')]
	private ?int $id = null;

	#[ORM\ManyToOne(targetEntity: User::class)]
	#[Groups('read')]
	private ?User $user;

	#[ORM\Column(type: 'string', nullable: true)]
	#[Groups(['read','write'])]
	private ?string $description = '';

	#[ORM\Column(type: 'string')]
	#[Groups(['read','write'])]
	private ?string $status = 'CREATED';

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getUser(): ?User
	{
		return $this->user;
	}

	public function setUser(?User $user): void
	{
		$this->user = $user;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(?string $description): void
	{
		$this->description = $description;
	}

	public function getStatus(): ?string
	{
		return $this->status;
	}

	public function setStatus(?string $status): void
	{
		$this->status = $status;
	}

}
