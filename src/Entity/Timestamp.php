<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Timestamp
{
	#[ORM\Column(type: 'datetime', options: ["default" => "CURRENT_TIMESTAMP"])]
    protected ?\DateTime $createdAt;

	#[ORM\Column(type: 'datetime', nullable: true)]
    protected ?\DateTime $updatedAt = null;

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;
    }

	#[ORM\PrePersist]
    public function onCreate()
    {
        $this->createdAt = new \DateTime();
    }

	#[ORM\PreUpdate]
    public function onUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
}
