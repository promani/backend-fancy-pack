<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
class Document
{
	use Timestamp;

	#[ORM\Column(type: 'integer')]
	#[ORM\Id]
	#[ORM\GeneratedValue(strategy: 'AUTO')]
	#[Groups('read')]
	private ?int $id = null;

	#[ORM\Column(type: 'string')]
	#[Groups(['read','write'])]
	private ?string $title = '';

	#[ORM\ManyToOne(targetEntity: User::class)]
	#[Groups('read')]
	private ?User $user;

	#[ORM\Column(type: 'string', nullable: true)]
	#[Groups(['read','write'])]
	private ?string $description = '';

	#[ORM\Column(type: 'string')]
	#[Groups(['read','write'])]
	private ?string $url = '/uploads/file.pdf';

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function setTitle(?string $title): void
	{
		$this->title = $title;
	}

	public function getUser(): ?User
	{
		return $this->user;
	}

	public function setUser(?User $user): void
	{
		$this->user = $user;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(?string $description): void
	{
		$this->description = $description;
	}

	public function getUrl(): ?string
	{
		return $this->url;
	}

	public function setUrl(?string $url): void
	{
		$this->url = $url;
	}

}
