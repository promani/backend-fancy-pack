<?php


namespace App\Controller;

use App\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
	#[Route('/documents', name: 'documentos')]
	public function index(): Response
	{
		return $this->json($this->getDoctrine()->getRepository(Document::class)->findBy(['name'=>'dasd']));
	}

	#[Route('/documents/{id}', name: 'documentos')]
	public function show($id): Response
	{
		return $this->json($this->getDoctrine()->getRepository(Document::class)->find($id));
	}

	#[Route('/documents', name: 'documentos')]
	public function create(Request $request): Response
	{
		$content = $request->getContent();
		$document = new Document();
		$document->setTitle($content['title']);
		// Upload document $request->getFiles()

		$this->getDoctrine()->getManager()->persist($document);
		$this->getDoctrine()->getManager()->flush();

		return $this->json();
	}
}
