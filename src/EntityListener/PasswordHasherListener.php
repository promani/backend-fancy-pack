<?php

namespace App\EntityListener;

use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class PasswordHasherListener
{
	public function __construct(private UserPasswordHasherInterface $encoder)
	{}

    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $user = $eventArgs->getEntity();

        if ($user instanceof User) {
	        $hashedPassword = $this->encoder->hashPassword($user, $user->getPassword());
	        $user->setPassword($hashedPassword);
        }
    }
}
