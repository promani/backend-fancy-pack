<?php

namespace Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class Test extends WebTestCase
{
	public function testSomething(): void
	{
		$client = static::createClient();

		$crawler = $client->request('GET', '/documents');

		$this->assertResponseIsSuccessful();
		$res_array = (array)json_decode($crawler->content());

		// Hacer un for
		$this->assertArrayHasKey('title', $res_array);
		$this->assertArrayHasKey('url', $res_array);

		$crawler = $client->request('GET', $res_array['url']);
		$this->assertResponseIsSuccessful();
		// chequear los headers
	}

	public function testCreate(): void
	{
		$client = static::createClient();

		$crawler = $client->request('POST', '/documents', [
			'title' => 'Ejemplo',
		], file_get_contents('../stubs/ejemplo.pdf'));

		$this->assertResponseStatusCodeSame(201);
	}

	public function testAnother(): void
	{
		$client = static::createClient();

		$crawler = $client->request('GET', '/documents/1');

		$this->assertResponseIsSuccessful();
		$res_array = (array)json_decode($crawler->content());

		$this->assertArrayHasKey('title', $res_array);
		$this->assertArrayHasKey('url', $res_array);

		$crawler = $client->request('GET', $res_array['url']);
		$this->assertResponseIsSuccessful();
	}
}
